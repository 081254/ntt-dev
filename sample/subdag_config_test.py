from airflow.models import DAG
import datetime as dt
from collections import OrderedDict
from airflow_template_helper.task_helper import build_sql_tasks_info
from airflow_template_helper.helper import build_tasks
from airflow_template_helper import constants
from airflow_template_helper.constants import SNOWFLAKE_CONNECTION_NAME, WASB_CONNECTION_NAME, SOURCE_CONTAINER, \
    ARCHIVE_CONTAINER, STAGE_NAME

SUBDAG_NAME = "subdag_config_test"
TASK_CONFIG_TABLE = "DEV.CORP_CONFIG.AIRFLOW_TASK_CONFIG"
SNOWFLAKE_CONNECTION_NAME = "snowflake_conn" 
SOURCE_FILE_PATH = ""
SOURCE_BASE_PATH = ""
ARCHIVE_BASE_PATH = ""



# Using
def build_subdag(master_dag, default_args, schedule_interval):
    dag_id = "{0}.{1}".format(master_dag, SUBDAG_NAME)
    with DAG(dag_id=dag_id,
             default_args=default_args,
             catchup=False,
             schedule_interval=schedule_interval) as dag:
        build_tasks(source_container=SOURCE_CONTAINER,
                    archive_container=ARCHIVE_CONTAINER,
                    source_file_path=SOURCE_FILE_PATH,
                    source_base_path=SOURCE_BASE_PATH,
                    archive_base_path=ARCHIVE_BASE_PATH,
                    sql_tasks_info=build_sql_tasks_info(SUBDAG_NAME,TASK_CONFIG_TABLE,SNOWFLAKE_CONNECTION_NAME,prod_move=False),
                    wasb_conn=WASB_CONNECTION_NAME,
                    snowflake_conn=SNOWFLAKE_CONNECTION_NAME,
                    include_blob_tasks=False)
        return dag
