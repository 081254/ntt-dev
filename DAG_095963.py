"""Very simple example DAG"""

import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

# Snowflake "plugins" 
from airflow.contrib.hooks.snowflake_hook import SnowflakeHook
from airflow.contrib.operators.snowflake_operator import SnowflakeOperator


#Defining our default arguments
args = {
    'owner': '095963',
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['marlonholland7@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
}

#Creating our dag object
dag = DAG(
    dag_id="TEST_095963", 
    default_args=args, 
    schedule_interval=None
)


#SQL query being used to insert data from our Azure storage account into our newly created table
insert_query = [
    """USE WAREHOUSE DEMO_WH;""",
    """USE DATABASE EDW_WALKTHROUGH;""",
    """USE SCHEMA PUBLIC;""",
    """COPY INTO TEST_095963
    FROM @EDW_TEST_STAGE
    FILES = ('Sample_095963.csv')
    file_format = (format_name = CSV_FILE_FORMAT)
    FORCE = TRUE;"""
]


#Using a with statement to execute cleanup code
with dag:

    #Using a snowflake operator to execute our insert command in Snowflake
    insert = SnowflakeOperator(
        task_id = "snowflake_insert",
        sql = insert_query,
        snowflake_conn_id = "snowflake_conn",
    )

insert 

#Using python bitwise shift operators to determine the workflow of our dag (the order in which our tasks will executed)
# insert >> # Following task would be seen here