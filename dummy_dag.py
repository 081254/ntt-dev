from datetime import datetime
import logging
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.base_hook import BaseHook
from airflow.models.connection import Connection

def connection_test():
    snowflake_login = BaseHook.get_connection('snowflake_conn').login
    snowflake_password = BaseHook.get_connection('snowflake_conn').password
    print(snowflake_login)
    logging.info(snowflake_login)
    print(snowflake_password)
    logging.info(snowflake_password)

dag = DAG('connection_parameter_test', 
    description='Simple tutorial DAG',
    schedule_interval='0 12 * * *',
    start_date=datetime(2017, 3, 20), 
    catchup=False)

dummy_operator = DummyOperator(task_id='dummy_task', retries=3, dag=dag)

conn_test_operator = PythonOperator(task_id='test_connection_parameters', python_callable=connection_test, dag=dag)

dummy_operator >> conn_test_operator