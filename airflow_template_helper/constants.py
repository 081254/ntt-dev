from airflow.models import Variable

# CONSTANTS used internally
FILE_LIST_XCOM_KEY = "file_list"
GET_FILE_LIST_TASK_ID = "get_list_of_files"
DAG_ERROR_EMAIL = ""

SNOWFLAKE_PIPE_CSV_FILE_FORMAT="corp_stg.FF_CSV_PIPE"
STAGE_NAME="PUBLIC.CORP_ADLS_SI_STAGE_LANDING"
SOURCE_CONTAINER = "airflow-test"
ARCHIVE_CONTAINER = "airflow-test"
SNOWFLAKE_CONNECTION_NAME = "snowflake_conn" 
WASB_CONNECTION_NAME = "wasb_conn"
SOURCE_BASE_PATH="eit/snowflake-stage/"
ARCHIVE_BASE_PATH="eit/snowflake-stage-processed/"
TASK_CONFIG_TABLE="DEV.CORP_CONFIG.AIRFLOW_TASK_CONFIG"

# SNOWFLAKE_PIPE_CSV_FILE_FORMAT = Variable.get("SNOWFLAKE_PIPE_CSV_FILE_FORMAT")
# STAGE_NAME = Variable.get("SNOWFLAKE_ADLS_STAGE_NAME")
# SOURCE_CONTAINER = Variable.get("ADLS_SOURCE_CONTAINER")
# ARCHIVE_CONTAINER = Variable.get("ADLS_ARCHIVE_CONTAINER")
# SNOWFLAKE_CONNECTION_NAME = Variable.get("AIRFLOW_SNOWFLAKE_CONNECTION_NAME")
# WASB_CONNECTION_NAME = Variable.get("AIRFLOW_WASB_CONNECTION_NAME")
# SOURCE_BASE_PATH = Variable.get("ADLS_SOURCE_BASE_PATH")
# ARCHIVE_BASE_PATH = Variable.get("ADLS_ARCHIVE_BASE_PATH")


GET_ON_DEMAND_TRIGGER_SQL = "select * from corp_config.config_airflow_subdags where dag_name='{0}'"
# the below table should be renamed once we have the final schema and  table name for config
CLEAR_ON_DEMAND_TRIGGER_SQL = "update corp_config.config_airflow_subdags set ON_DEMAND_TRIGGER=FALSE where dag_name='{0}'"
