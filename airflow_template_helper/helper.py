from docutils.nodes import target
from airflow.contrib.hooks.wasb_hook import WasbHook
from airflow_template_helper.constants import FILE_LIST_XCOM_KEY, GET_FILE_LIST_TASK_ID, STAGE_NAME, SNOWFLAKE_CONNECTION_NAME
from airflow.operators.python_operator import PythonOperator, ShortCircuitOperator
from airflow.contrib.hooks.snowflake_hook import SnowflakeHook
import logging

def get_file_list_snowflake(source_base_prefix, source_file_path,
                            snowflake_conn_name=None, stage_name=None, pattern=None, **context):

    if not stage_name:
        stage_name = STAGE_NAME

    if not snowflake_conn_name:
        snowflake_conn_name = SNOWFLAKE_CONNECTION_NAME

    adls_path = get_source_file_path(source_base_prefix=source_base_prefix, source_file_path=source_file_path)

    if pattern:
        sql_statement = """list @{0}/{1} pattern='{2}'""".format(stage_name, adls_path, pattern)
    else:
        sql_statement = """list @{0}/{1}""".format(stage_name, adls_path)

    print("SQL Statement for listing files:", sql_statement)
    snowflake_hook = SnowflakeHook(snowflake_conn_id=snowflake_conn_name,
                                   warehouse=context.get("warehouse"),
                                   schema=context.get("schema"),
                                   database=context.get("database"),
                                   role=context.get("role"))

    files_list = []
    try:
        conn = snowflake_hook.get_conn()
        cursor = conn.cursor()
        cursor.execute(sql_statement)

        for row in cursor:
            print(row[0])
            files_list.append(row[0])
    finally:
        if cursor:
            cursor.close()
        conn.close()

    logging.info("file list : {0}".format(files_list))

    task_instance = context['ti']
    task_instance.xcom_push(FILE_LIST_XCOM_KEY, files_list)
    if len(files_list) > 0:
        task_instance.xcom_push("no_of_files_available", len(files_list))


def get_file_list(source_container, source_file_path, source_base_path, wasb_connection_name, **context):
    """
    Gets the list of files available in the azure blob location specified by parameters and stores them in the xcom
    key "no_of_files_available".
    :param source_container: The source container from which the list of file are retrieved.
    :param source_file_path: The path within the source container to retrieve file list
    :param wasb_connection_name: The airflow WASB connection name.
    :param context: Context provided by Airflow.
    :return:
    """
    wasb_hook = WasbHook(wasb_conn_id=wasb_connection_name)
    block_blob_service = wasb_hook.get_conn()

    prefix = source_file_path
    if source_base_path and source_base_path.strip() != "":
        prefix = source_base_path.strip() + source_file_path

    blobs = block_blob_service.list_blobs(container_name=source_container, prefix=prefix)
    file_list = [blob.name for blob in blobs if blob.properties.content_length != 0]
    logging.info("file list : {0}".format(file_list))
    task_instance = context['ti']
    task_instance.xcom_push(FILE_LIST_XCOM_KEY, file_list)
    if len(file_list) > 0:
        task_instance.xcom_push("no_of_files_available", len(file_list))


def decide_to_move_ahead(**context):

    """
    Used in the ShortCircuit operator to decide whether to move ahead with other tasks
     based on the number of files available for processing.
    :param context: Provided by Airflow.
    :return:
    """

    task_instance = context['ti']
    value = task_instance.xcom_pull(task_ids=GET_FILE_LIST_TASK_ID, key="no_of_files_available")

    if value:
        return True
    else:
        return False


def get_sql_execution_operator(task_id, sql, snowflake_conn_name):
    """
    Constructs the PythonOperator to execute a SQL statement with the help of python function execute_sql
    :param task_id: Task Id to be used for the Airflow task.
    :param sql: The SQL statement to be executed.
    :param snowflake_conn_name: The Airflow connection name for snowflake.
    :return:
    """
    sql_statement = None
    warehouse = None
    schema = None
    database = None
    role = None
    xcom_key = None

    if isinstance(sql, dict):
        sql_statement = sql.get("sql")
        warehouse = sql.get("warehouse")
        schema = sql.get("schema")
        database = sql.get("database")
        role = sql.get("role")
        xcom_key = sql.get("xcom_key")

    else:
        sql_statement = sql

    return PythonOperator(task_id=task_id,
                          python_callable=execute_sql,
                          provide_context=True,
                          op_kwargs={"sql_statement": sql_statement,
                                     "snowflake_conn_name": snowflake_conn_name,
                                     "xcom_key": xcom_key,
                                     "warehouse": warehouse,
                                     "database": database,
                                     "schema": schema,
                                     "role": role})


def execute_sql(sql_statement, snowflake_conn_name, xcom_key=None, **context):
    """
    The python callable to be used within the PythonOperator for executing a SQL. This function executes SQL statement
    and logs the result, optionally storing the result in the XCOM key.
    :param sql_statement: SQL statement to be executed for a task.
    :param snowflake_conn_name: Snowflake connection name
    :param xcom_key: XCOM key to be used if the results need to be inserted into XCOM.
    :param context: Provided by Airflow.
    :return:
    """
    snowflake_hook = SnowflakeHook(snowflake_conn_id=snowflake_conn_name,
                                   warehouse=context.get("warehouse"),
                                   schema=context.get("schema"),
                                   database=context.get("database"),
                                   role=context.get("role"))
    conn = snowflake_hook.get_conn()
    try:
        cursor = conn.cursor()
        cursor.execute(sql_statement)
        field_names = [name[0] for name in cursor.description]
        result_log = []

        for _, row in enumerate(cursor):
            row_returned = list(zip(field_names, row))
            result_log.append(dict(row_returned))

        # Log the rows
        logging.info(result_log)

        if xcom_key:
            task_instance = context['ti']
            task_instance.xcom_push(xcom_key, result_log)

    finally:
        if cursor:
            cursor.close()
        conn.close()
        logging.info("is connection closed:{0}".format(conn.is_closed()))
        logging.info("is cursor closed: {0}".format(cursor.is_closed()))


def archive_processed_files(wasb_conn, source_container, archive_container,
                            source_base_path,
                            archive_base_path = None,
                            **context):
    """
    A python function to move the processed files to the archive.
    :param wasb_conn: WASB connection name
    :param source_container: Source Container
    :param archive_container: Archive Container
    :param context: Provided by Airflow.
    :return:
    """

    task_instance = context['ti']
    file_list = task_instance.xcom_pull(key=FILE_LIST_XCOM_KEY, task_ids=GET_FILE_LIST_TASK_ID)

    wasb_hook = WasbHook(wasb_conn_id=wasb_conn)
    block_blob_service = wasb_hook.get_conn()

    # copy files to the archie container from the source container
    for blob in file_list:
        url = block_blob_service.make_blob_url(container_name=source_container,
                                               blob_name=blob,
                                               sas_token=block_blob_service.sas_token[1:])

        logging.info("About to copy the blob {0}/{1}".format(source_container, blob))
        target_blob_name = blob
        if archive_base_path and archive_base_path.strip() != '':
            if source_base_path and source_base_path.strip() != "":
                target_blob_name = blob.replace(source_base_path, '')
            target_blob_name = archive_base_path.strip() + target_blob_name

        copy_props = block_blob_service.copy_blob(container_name=archive_container,
                                                  blob_name=target_blob_name,
                                                  copy_source=url,
                                                  requires_sync=True)
        logging.info("Copying status for {0}/{1} is {2}".format(source_container, blob, copy_props.status))

        # once the copy is done, move the file to archived location.
        # Batch delete is not supported for hierachical file system using azure blob API,
        # falling back to individual deletes
        logging.info("About to delete the blob {0}/{1}".format(source_container, blob))
        block_blob_service.delete_blob(container_name=source_container, blob_name=blob)


def get_source_file_path(source_base_prefix, source_file_path):
    return_path = source_file_path
    if source_base_prefix and source_base_prefix.strip() != '':
        return_path = source_base_prefix + source_file_path
    return return_path


def build_tasks(source_container,
                source_file_path,
                archive_container,
                wasb_conn,
                snowflake_conn,
                sql_tasks_info,
                source_base_path=None,
                archive_base_path=None,
                include_blob_tasks=True,
                pattern=None):
    """
    The critical method to build the list of tasks and dependencies based on the parameters provided.

    :param source_container: Source container in ADLS Gen2 or Azure blob
    :param source_file_path: Source file path in ADLS Gen2 or prefix in Azure Blob
    :param archive_container: Archive Container in ADLS Gen2
    :param source_base_path: base path till the snowlfake-stage area for source files
    :param archive_base_path: base path till the snowflake-stage-processed for archiving files
    :param wasb_conn: WASB Connection name
    :param snowflake_conn: Snowflake Connection name
    :param sql_tasks_info: SQL tasks defined as a list containing either string (a SQL) or dictionary.
    :param include_blob_tasks: This gives an option to whether include blob related tasks, like getting list of files,
     archive files in ADLS Gen2
    :param pattern: Pattern to search for the files

    :return:
    """

    if include_blob_tasks:
        list_files_task = PythonOperator(
            task_id=GET_FILE_LIST_TASK_ID,
            python_callable=get_file_list_snowflake,
            op_kwargs={
                "source_base_prefix": source_base_path,
                "source_file_path": source_file_path,
                "snowflake_conn_name": snowflake_conn,
                "pattern": pattern
            },
            provide_context=True
        )

        decide_to_move_ahead_task = ShortCircuitOperator(
            task_id="decide_to_move_ahead_or_not",
            python_callable=decide_to_move_ahead,
            provide_context=True)

        list_files_task >> decide_to_move_ahead_task

    sql_tasks = [get_sql_execution_operator(task_id, sql, snowflake_conn)
                 for (task_id, sql) in sql_tasks_info.items()]

    for index, sql_task in enumerate(sql_tasks):
        if index > 0:
            sql_tasks[index - 1] >> sql_tasks[index]
        else:
            if include_blob_tasks:
                decide_to_move_ahead_task >> sql_tasks[index]

    if include_blob_tasks:
        move_files_to_archive_task = PythonOperator(
            task_id="move_files_to_archive",
            python_callable=archive_processed_files,
            provide_context=True,
            op_kwargs={
                "wasb_conn": wasb_conn,
                "source_container": source_container,
                "archive_container": archive_container,
                "archive_base_path": archive_base_path,
                "source_base_path": source_base_path
            }
        )

        sql_tasks[-1] >> move_files_to_archive_task
