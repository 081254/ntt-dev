from collections import OrderedDict
from airflow.contrib.hooks.snowflake_hook import SnowflakeHook

"""
Author: Marlon Holland
"""


def build_sf_query(col, dag_name, config_table):
    """Builds a SQL query meant for grabbing data from Snowflake.

    :param col: The target column to be fetched from a snowflake table
    :type col: str
    :param dag_name: The DAG name maintained in the snowflake table
    :type dag_name: str
    :param config_table: The configuration table in snowflake <DB>.<SCHEMA>.<TABLENAME>
    :type config_table: str
    :returns: a string representing the query to be run in Snowflake
    :rtype: string
    """

    if 'PROD_OBJECT_CONFIG' in config_table:
        return "SELECT {} FROM {} WHERE DAG_NAME = '{}' AND PROCESSED IS NULL".format(col,config_table,dag_name)
    elif 'AIRFLOW_TASK_CONFIG' in config_table:
        return "SELECT {} FROM {} WHERE DAG_NAME = '{}'".format(col,config_table,dag_name)

def build_update_query(dag_name, task_name, config_table):
    """Builds a SQL query meant for updating records in Snowflake. Only used in prod object moves.

    :param dag_name: The DAG name maintained in the snowflake table
    :type dag_name: str
    :param task_name: The task name that has been completed and needs to be marked as processed
    :type task_name: str
    :param config_table: The configuration table in snowflake <DB>.<SCHEMA>.<TABLENAME>
    :type config_table: str
    :returns: a string representing the query to be run in Snowflake
    :rtype: string
    """

    return "UPDATE {} SET PROCESSED = 'X', PROCESSED_DATE = current_timestamp() WHERE DAG_NAME = '{}' AND TASK_NAME = '{}';".format(config_table, dag_name, task_name)
    
def get_sf_info(sf_info, dag_name, config_table, snowflake_conn_id):
    """Connects to snowflake and runs a select query on one column.

    :param sf_info: The target column to be fetched
    :type sf_info: str
    :param dag_name: The DAG name maintained in the snowflake table
    :type dag_name: str
    :param config_table: The configuration table in snowflake <DB>.<SCHEMA>.<TABLENAME>
    :type config_table: str
    :param snowflake_conn_id: The name of the snowflake connection in the airflow admin tab.
    :type snowflake_conn_id: str
    :returns: a list containing the result records within Tuples
    :rtype: list
    """
    
    sf_hook = SnowflakeHook(snowflake_conn_id=snowflake_conn_id)
    sf_info = sf_hook.get_records(build_sf_query(sf_info, dag_name, config_table))
    return sf_info
    
def build_sql_tasks_info(dag_name, config_table, snowflake_conn_id, prod_move = False):
    """The primary function of the module. Gathers all the task names and corresponding sql queries to be processed.

    :param dag_name: The DAG name maintained in the snowflake table
    :type dag_name: str
    :param config_table: The configuration table in snowflake <DB>.<SCHEMA>.<TABLENAME>
    :type config_table: str
    :param task_name: The task name that has been completed and needs to be marked as processed
    :type task_name: str
    :param snowflake_conn_id: The name of the snowflake connection in the airflow admin tab.
    :type snowflake_conn_id: str
    :param prod_move: A boolean stating whether this DAG is involved in moving objects to production.
        (default is False)
    :type prod_move: bool
    :returns: an ordered dictionary containing all the task names and corresponding sql queries.
    :rtype: OrderedDict
    """

    sql_task_list = []
    sql_tasks_info = OrderedDict()
    task_names = get_sf_info('task_name', dag_name, config_table, snowflake_conn_id)
    sql_tasks = get_sf_info('sql_query', dag_name, config_table, snowflake_conn_id)
    
    for sql_task in sql_tasks:
        sql_task_list.append("".join(sql_task[0]).split(';')[0] + ';')
        
    for index, task_name in enumerate(task_names):
        task_name_str = "".join(task_names[index])
        sql_tasks_info[task_name_str] = sql_task_list[index]

        if prod_move:
            sql_tasks_info["CONFIG_TABLE_UPDATE_FOR_TASK-{}".format(task_name_str)] = build_update_query(dag_name, task_name_str, config_table)

    return sql_tasks_info

