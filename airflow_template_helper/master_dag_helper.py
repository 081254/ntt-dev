import importlib
import os
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import ShortCircuitOperator, BranchPythonOperator
from airflow.operators.subdag_operator import SubDagOperator
from airflow_template_helper import helper, constants

def load_sub_dags(subdags_folder):
    # Changed file list string to look in NTT's bitnami server directory (Compared to Astronomer's directory)
    file_list = os.listdir("/opt/bitnami/airflow/dags/" + subdags_folder)
    # file_list = os.listdir("dags/" + subdags_folder)
    python_files = []
    cwd = os.getcwd()
    print("cwd:", cwd)

    for file in file_list:
        if file != '__init__.py' and file != '__pycache__':
            python_files.append(file)
    print("python files:", python_files)

    return [importlib.import_module(subdags_folder.replace("/", ".") + "." + python_file.replace(".py", ""))
            for python_file in sorted(python_files)]


def check_if_manual_trigger(**context):
    dag_run = context['dag_run']
    if dag_run.external_trigger:
        return True
    else:
        return False


def get_xcom_config_name(dag_name):
    return dag_name + "_CONFIG"


def check_run_schedule_for_subdag(dag_name, skip_task_id, subdag_id, **context):
    dag_run = context['dag_run']
    if dag_run.external_trigger:
        print("it's an external trigger...may need to check")
        task_instance = context['ti']
        config = task_instance.xcom_pull(key=get_xcom_config_name(dag_name=dag_name),
                                         task_ids='GET_SUBDAGS_ON_DEMAND_FLAGS_TASK')
        print("on-demand config:", config)

        for config_item in config:
            print("config item in loop:", config_item)
            if config_item["DAG_NAME"].upper() == dag_name.upper() and \
               config_item["SUB_DAG_NAME"].upper() == subdag_id.upper() and \
               config_item["ON_DEMAND_TRIGGER"]:
                print("on demand trigger found for {0}".format(subdag_id))
                return subdag_id

        return skip_task_id
    return subdag_id


def build_master_subdags(master_dag_name, subdags_folder, default_args, schedule_interval):
    mods = load_sub_dags(subdags_folder)
    subdags = []

    get_config_task = helper.get_sql_execution_operator(task_id='GET_SUBDAGS_ON_DEMAND_FLAGS_TASK', sql={
        'sql': constants.GET_ON_DEMAND_TRIGGER_SQL.format(master_dag_name),
        'xcom_key': get_xcom_config_name(dag_name=master_dag_name)
    }, snowflake_conn_name=constants.SNOWFLAKE_CONNECTION_NAME)

    for mod in mods:
        subdags.append(
            SubDagOperator(subdag=mod.build_subdag(master_dag=master_dag_name,
                                                   default_args=default_args,
                                                   schedule_interval=schedule_interval),
                           task_id=mod.SUBDAG_NAME,
                           default_args=default_args,
                           trigger_rule='all_done',
                           retries=0)
        )

    previous_dummy = None
    previous_subdag = None
    no_of_subdags = len(subdags)

    for index, subdag in enumerate(subdags):
        # first build a python branch operator
        dummy_id = subdag.task_id + "_IGNORE"
        branch_id = subdag.task_id + "_CHECK"

        branch = BranchPythonOperator(task_id=branch_id,
                                      python_callable=check_run_schedule_for_subdag,
                                      provide_context=True,
                                      trigger_rule='all_done',
                                      op_kwargs={
                                          'dag_name': master_dag_name,
                                          'skip_task_id': dummy_id,
                                          'subdag_id': subdag.task_id
                                      })
        dummy = DummyOperator(task_id=dummy_id)

        branch >> [subdag, dummy]
        print("{0} >> [{1}, {2}]".format(branch.task_id, subdag.task_id, dummy.task_id))

        if index > 0:
            print("trying to create dependency: [{0}, {1}] >> {2}".format(previous_subdag.task_id, previous_dummy.task_id, branch.task_id))
            [previous_subdag, previous_dummy] >> branch
        else:
            get_config_task >> branch

        previous_dummy = dummy
        previous_subdag = subdag

    check_if_manual_trigger_task = ShortCircuitOperator(task_id="CHECK_IF_MANUAL_TRIGGER_TASK",
                                                        python_callable=check_if_manual_trigger,
                                                        provide_context=True,
                                                        trigger_rule='all_done')

    [subdag, dummy] >> check_if_manual_trigger_task

    clear_config_task = helper.get_sql_execution_operator(task_id='CLEAR_SUBDAGS_ON_DEMAND_FLAGS_TASK', sql={
        'sql': constants.CLEAR_ON_DEMAND_TRIGGER_SQL.format(master_dag_name)
    }, snowflake_conn_name=constants.SNOWFLAKE_CONNECTION_NAME)

    check_if_manual_trigger_task >> clear_config_task


