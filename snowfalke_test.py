import airflow 
from airflow import DAG
import datetime as dt
from airflow.contrib.operators.snowflake_operator import SnowflakeOperator

args = {
    'owner': 'ntt',
    'start_date': dt.datetime(year=2020, month=6, day=7),
    'email': ['sindhura.bhattam@nttdata.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
}
dag = DAG(
    dag_id="Snowflake_Test",
    default_args=args,
    schedule_interval = '@hourly'
)
# Create Transient Table for Demo
create_transient = """
CREATE OR REPLACE TABLE "DEV"."CORP_MASTER"."QV_INV_DETAIL_INFA_TRNS_test" ( 
SOURCE_SYSTEM NVARCHAR(30) ,
MANDT NVARCHAR(3) ,
FISCPER NVARCHAR(5000) ,
FISCAL_YEAR_WEEK_STR NVARCHAR(6) ,
BUKRS NVARCHAR(15) ,
WERKS NVARCHAR(12) ,
PRCTR VARCHAR(10) ,
MATNR NVARCHAR(90) ,
SOBKZ NVARCHAR ,
BWTAR NVARCHAR(30) ,
WAERS NVARCHAR(5) ,
CM_UNREST_STOCK_QTY DECIMAL(13,3) ,
CM_QI_STOCK_QTY DECIMAL(13,3) ,
CM_BLK_STK_QTY DECIMAL(13,3) ,
CM_CUST_CONSG_TOT_QTY DECIMAL(20,3) ,
CM_VEND_CONSG_TOT_QTY DECIMAL(13,3) ,
CM_SIT_STOCK_QTY DECIMAL(20,3) ,
CM_UNREST_STOCK_VALUE DECIMAL(34,7) ,
CM_QI_STOCK_VALUE DECIMAL(34,7) ,
CM_BLK_STK_VALUE DECIMAL(34,7) ,
CM_CUST_CONSG_TOT_VALUE DECIMAL(34,7) ,
CM_VEND_CONSG_TOT_VALUE DECIMAL(34,7) ,
CM_SIT_STOCK_VALUE DECIMAL(34,7) ,
SNAPSHOT_DTH NVARCHAR(30),
MD5 NVARCHAR(5000),
CREATE_PROCESS_TS TIMESTAMP_NTZ(9),
CONSTRAINT PK_QV_INV_DETAIL_INFA_TRNS primary key (SOURCE_SYSTEM,MANDT,FISCPER,FISCAL_YEAR_WEEK_STR,BUKRS,WERKS,PRCTR,MATNR,SOBKZ,BWTAR,WAERS)
);;
"""
# Load Data - from ADLS to transient Table
Load_data_ADLS_to_Trns = """
COPY INTO "DEV"."CORP_MASTER"."QV_INV_DETAIL_INFA_TRNS_test"
    FROM (
    SELECT $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,
    current_timestamp:: timestamp
    FROM
    @PUBLIC.CORP_ADLS_SI_STAGE_LANDING/src_hana_bhp/inventory/QV_INV_DETAIL_ADLSGEN2
    )
FILE_FORMAT = (FORMAT_NAME = PUBLIC.CSV_SEMI_COLON)
"""
# Get Data in Stage using upsert
Merge_data_Trns_to_Stg = """
    MERGE INTO HON_ENT_SAP_SOURCE_DB.PUBLIC.QV_INV_DETAIL_INFA_Stg AS Stg 
USING DEV.CORP_MASTER.QV_INV_DETAIL_INFA_TRNS_test AS Trns
 on Trns.SOURCE_SYSTEM = Stg.SOURCE_SYSTEM and Trns.MATNR = Stg.MATNR and Trns.MANDT = Stg.MANDT and Trns.FISCPER = Stg.FISCPER and Trns.FISCAL_YEAR_WEEK_STR = Stg.FISCAL_YEAR_WEEK_STR
   and Trns.BUKRS = Stg.BUKRS and Trns.WERKS = Stg.WERKS and Trns.PRCTR = Stg.PRCTR and ifnull(Trns.SOBKZ,'') = ifnull(Stg.SOBKZ,'')
   and ifnull(Trns.BWTAR,'') = ifnull(Stg.BWTAR,'') and ifnull(Trns.WAERS,'') = ifnull(Stg.WAERS,'')
 WHEN MATCHED AND
     (
       Stg.MD5 <> Trns.MD5
     )
     THEN UPDATE SET
        Stg.CM_UNREST_STOCK_QTY = Trns.CM_UNREST_STOCK_QTY,
        Stg.CM_QI_STOCK_QTY = Trns.CM_QI_STOCK_QTY,
        Stg.CM_BLK_STK_QTY = Trns.CM_BLK_STK_QTY,
        Stg.CM_CUST_CONSG_TOT_QTY = Trns.CM_CUST_CONSG_TOT_QTY,
        Stg.CM_VEND_CONSG_TOT_QTY = Trns.CM_VEND_CONSG_TOT_QTY,
        Stg.CM_SIT_STOCK_QTY = Trns.CM_SIT_STOCK_QTY,
        Stg.CM_UNREST_STOCK_VALUE = Trns.CM_UNREST_STOCK_VALUE,
        Stg.CM_QI_STOCK_VALUE = Trns.CM_QI_STOCK_VALUE,
        Stg.CM_BLK_STK_VALUE = Trns.CM_BLK_STK_VALUE,
        Stg.CM_CUST_CONSG_TOT_VALUE = Trns.CM_CUST_CONSG_TOT_VALUE,
        Stg.CM_VEND_CONSG_TOT_VALUE = Trns.CM_VEND_CONSG_TOT_VALUE,
        Stg.CM_SIT_STOCK_VALUE = Trns.CM_SIT_STOCK_VALUE,
        Stg.SNAPSHOT_DTH = Trns.SNAPSHOT_DTH,
        Stg.MD5 = Trns.MD5,
        Stg.UPDATE_PROCESS_TS = current_timestamp::timestamp
 WHEN NOT MATCHED
     THEN INSERT (
        SOURCE_SYSTEM
        ,MANDT
        ,FISCPER
        ,FISCAL_YEAR_WEEK_STR
        ,BUKRS
        ,WERKS
        ,PRCTR
        ,MATNR
        ,SOBKZ
        ,BWTAR
        ,WAERS
        ,CM_UNREST_STOCK_QTY
        ,CM_QI_STOCK_QTY
        ,CM_BLK_STK_QTY
        ,CM_CUST_CONSG_TOT_QTY
        ,CM_VEND_CONSG_TOT_QTY
        ,CM_SIT_STOCK_QTY
        ,CM_UNREST_STOCK_VALUE
        ,CM_QI_STOCK_VALUE
        ,CM_BLK_STK_VALUE
        ,CM_CUST_CONSG_TOT_VALUE
        ,CM_VEND_CONSG_TOT_VALUE
        ,CM_SIT_STOCK_VALUE
        ,SNAPSHOT_DTH
        ,MD5
        ,CREATE_PROCESS_TS
       )
      values(
        Trns.SOURCE_SYSTEM
        ,Trns.MANDT
        ,Trns.FISCPER
        ,Trns.FISCAL_YEAR_WEEK_STR
        ,Trns.BUKRS
        ,Trns.WERKS
        ,Trns.PRCTR
        ,Trns.MATNR
        ,ifnull(Trns.SOBKZ,'')
        ,ifnull(Trns.BWTAR,'')
        ,Trns.WAERS
        ,Trns.CM_UNREST_STOCK_QTY
        ,Trns.CM_QI_STOCK_QTY
        ,Trns.CM_BLK_STK_QTY
        ,Trns.CM_CUST_CONSG_TOT_QTY
        ,Trns.CM_VEND_CONSG_TOT_QTY
        ,Trns.CM_SIT_STOCK_QTY
        ,Trns.CM_UNREST_STOCK_VALUE
        ,Trns.CM_QI_STOCK_VALUE
        ,Trns.CM_BLK_STK_VALUE
        ,Trns.CM_CUST_CONSG_TOT_VALUE
        ,Trns.CM_VEND_CONSG_TOT_VALUE
        ,Trns.CM_SIT_STOCK_VALUE
        ,Trns.SNAPSHOT_DTH
        ,Trns.MD5
        ,current_timestamp::TIMESTAMP
);
"""
# Get Data into Final - to Snowflake
Merge_data_Stg_to_Tgt = """
MERGE INTO HON_ENT_SAP_SOURCE_DB.PUBLIC.QV_INV_DETAIL_INFA_TGT    AS Tgt
USING HON_ENT_SAP_SOURCE_DB.PUBLIC.QV_INV_DETAIL_INFA_STG     AS Stg
on Stg.SOURCE_SYSTEM = Tgt.SOURCE_SYSTEM and Stg.MATNR = Tgt.MATNR and Stg.MANDT = Tgt.MANDT and Stg.FISCPER = Tgt.FISCPER and Stg.FISCAL_YEAR_WEEK_STR = Tgt.FISCAL_YEAR_WEEK_STR
and Stg.BUKRS = Tgt.BUKRS and Stg.WERKS = Tgt.WERKS and Stg.PRCTR = Tgt.PRCTR and ifnull(Stg.SOBKZ,'') = ifnull(Tgt.SOBKZ,'')
   and ifnull(Stg.BWTAR,'') = ifnull(Tgt.BWTAR,'') and ifnull(Stg.WAERS,'') = ifnull(Tgt.WAERS,'')
 WHEN MATCHED AND
     (
       Tgt.MD5 <> Stg.MD5
      )
     THEN UPDATE SET
        Tgt.CM_UNREST_STOCK_QTY = Stg.CM_UNREST_STOCK_QTY,
        Tgt.CM_QI_STOCK_QTY = Stg.CM_QI_STOCK_QTY,
        Tgt.CM_BLK_STK_QTY = Stg.CM_BLK_STK_QTY,
        Tgt.CM_CUST_CONSG_TOT_QTY = Stg.CM_CUST_CONSG_TOT_QTY,
        Tgt.CM_VEND_CONSG_TOT_QTY = Stg.CM_VEND_CONSG_TOT_QTY,
        Tgt.CM_SIT_STOCK_QTY = Stg.CM_SIT_STOCK_QTY,
        Tgt.CM_UNREST_STOCK_VALUE = Stg.CM_UNREST_STOCK_VALUE,
        Tgt.CM_QI_STOCK_VALUE = Stg.CM_QI_STOCK_VALUE,
        Tgt.CM_BLK_STK_VALUE = Stg.CM_BLK_STK_VALUE,
        Tgt.CM_CUST_CONSG_TOT_VALUE = Stg.CM_CUST_CONSG_TOT_VALUE,
        Tgt.CM_VEND_CONSG_TOT_VALUE = Stg.CM_VEND_CONSG_TOT_VALUE,
        Tgt.CM_SIT_STOCK_VALUE = Stg.CM_SIT_STOCK_VALUE,
        Tgt.SNAPSHOT_DTH = Stg.SNAPSHOT_DTH,
        Tgt.MD5 = Stg.MD5,
        Tgt.UPDATE_PROCESS_TS = current_timestamp::timestamp
 WHEN NOT MATCHED
     THEN INSERT (
        SOURCE_SYSTEM
        ,MANDT
        ,FISCPER
        ,FISCAL_YEAR_WEEK_STR
        ,BUKRS
        ,WERKS
        ,PRCTR
        ,MATNR
        ,SOBKZ
        ,BWTAR
        ,WAERS
        ,CM_UNREST_STOCK_QTY
        ,CM_QI_STOCK_QTY
        ,CM_BLK_STK_QTY
        ,CM_CUST_CONSG_TOT_QTY
        ,CM_VEND_CONSG_TOT_QTY
        ,CM_SIT_STOCK_QTY
        ,CM_UNREST_STOCK_VALUE
        ,CM_QI_STOCK_VALUE
        ,CM_BLK_STK_VALUE
        ,CM_CUST_CONSG_TOT_VALUE
        ,CM_VEND_CONSG_TOT_VALUE
        ,CM_SIT_STOCK_VALUE
        ,SNAPSHOT_DTH
        ,MD5
        ,CREATE_PROCESS_TS
       )
      values(
        Stg.SOURCE_SYSTEM
        ,Stg.MANDT
        ,Stg.FISCPER
        ,Stg.FISCAL_YEAR_WEEK_STR
        ,Stg.BUKRS
        ,Stg.WERKS
        ,Stg.PRCTR
        ,Stg.MATNR
        ,ifnull(Stg.SOBKZ,'')
        ,ifnull(Stg.BWTAR,'')
        ,Stg.WAERS
        ,Stg.CM_UNREST_STOCK_QTY
        ,Stg.CM_QI_STOCK_QTY
        ,Stg.CM_BLK_STK_QTY
        ,Stg.CM_CUST_CONSG_TOT_QTY
        ,Stg.CM_VEND_CONSG_TOT_QTY
        ,Stg.CM_SIT_STOCK_QTY
        ,Stg.CM_UNREST_STOCK_VALUE
        ,Stg.CM_QI_STOCK_VALUE
        ,Stg.CM_BLK_STK_VALUE
        ,Stg.CM_CUST_CONSG_TOT_VALUE
        ,Stg.CM_VEND_CONSG_TOT_VALUE
        ,Stg.CM_SIT_STOCK_VALUE
        ,Stg.SNAPSHOT_DTH
        ,Stg.MD5
        ,current_timestamp::TIMESTAMP
);
"""

with dag:
    create_transient = SnowflakeOperator(
        task_id="create_transient",
        sql=create_transient,
        snowflake_conn_id="snowflake_conn"
    )

    Load_data_ADLS_to_Trns = SnowflakeOperator(
        task_id="Load_data_ADLS_to_Trns",
        sql=Load_data_ADLS_to_Trns,
        snowflake_conn_id="snowflake_conn"
    )
    Merge_data_Trns_to_Stg = SnowflakeOperator(
        task_id="Merge_data_Trns_to_Stg",
        sql=Merge_data_Trns_to_Stg,
        snowflake_conn_id="snowflake_conn"
    )
    Merge_data_Stg_to_Tgt = SnowflakeOperator(
        task_id="Merge_data_Stg_to_Tgt",
        sql=Merge_data_Stg_to_Tgt,
        snowflake_conn_id="snowflake_conn"
    )
create_transient >> Load_data_ADLS_to_Trns >> Merge_data_Trns_to_Stg >> Merge_data_Stg_to_Tgt