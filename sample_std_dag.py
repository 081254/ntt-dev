from airflow.models import DAG
import datetime as dt
from airflow_template_helper import master_dag_helper
from airflow_template_helper import constants

# include corp in the mater dag, maintain the DAG_NAME convention, example below
DAG_NAME = "sample_std_dag"
SCHEDULE_INTERVAL = "*/5 * * * *"
SUBDAGS_FOLDER = "sample"

default_args = {
    'owner': 'Marlon',
    'max_active_runs': 1,
    'start_date': dt.datetime(year=2020, month=1, day=1),
    'depends_on_past': False,
}


with DAG(dag_id=DAG_NAME,
         default_args=default_args,
         catchup=False,
         schedule_interval=SCHEDULE_INTERVAL,
         max_active_runs=1) as dag:
    master_dag_helper.build_master_subdags(master_dag_name=DAG_NAME,
                                           subdags_folder=SUBDAGS_FOLDER,
                                           default_args=default_args,
                                           schedule_interval=SCHEDULE_INTERVAL)