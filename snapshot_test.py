from airflow.models import DAG
import datetime as dt
from collections import OrderedDict
from airflow_template_helper.helper import build_tasks
from airflow_template_helper import constants
from airflow_template_helper.constants import SNOWFLAKE_CONNECTION_NAME, WASB_CONNECTION_NAME, SOURCE_CONTAINER, \
    ARCHIVE_CONTAINER, STAGE_NAME

DAG_NAME = "snapshot_test"
# SCHEDULE_INTERVAL = "@once"
SCHEDULE_INTERVAL = "30 11 06-10 * *"
SOURCE_FILE_PATH=""

sql_tasks_info = OrderedDict()
sql_tasks_info["delete_fact_snapshot_table"] ="""
DELETE FROM "TEST_DB"."ISC_TARGET"."FACT_INVENTORY_SNAPSHOT_MONTHLY" ;
"""
sql_tasks_info["insert_into_snapshot_table"] = """
INSERT INTO  "TEST_DB"."ISC_TARGET"."FACT_INVENTORY_SNAPSHOT_MONTHLY" (SOURCE_SYSTEM,SOURCE_ERP,COMPANY_CODE,SOURCE_ENTITY,SOURCE_ORG,PLANT_SK,STD_PRICE_AMT_LCL,PRICE_UNIT,MOVING_PRICE_AMT_LCL,COST_PER_UNIT_AMT_LCL,MATERIAL_NUM_SK,
                                PROFIT_CENTER,STORAGE_LOCATION,VALUATION_CLASS,VALUATION_TYPE,SPL_STOCK_IND,LCL_CURR,FISCAL_PERIOD,FISCAL_YEAR,TOTAL_STOCK_QTY,VALUATED_STOCK_QTY,
                                UNRESTRICTED_STOCK_QTY,QI_STOCK_QTY,BLOCKED_STOCK_QTY,IN_TRANSIT_QTY,RET_STOCK_QTY,CUST_CONSIGNED_QTY,VENDOR_CONSIGNED_QTY,RESTRICTED_STOCK_QTY,TOTAL_STOCK_AMT_LCL,
                                VALUATED_STOCK_AMT_LCL,UNRESTRICTED_STOCK_AMT_LCL,QI_STOCK_AMT_LCL,
                                BLOCKED_STOCK_AMT_LCL,IN_TRANSIT_AMT_LCL,RET_STOCK_AMT_LCL,CUST_CONSIGNED_AMT_LCL,VENDOR_CONSIGNED_AMT_LCL,RESTRICTED_STOCK_AMT_LCL,INV_LOAD_TS)
(SELECT SOURCE_SYSTEM,ANY_VALUE(SOURCE_ERP),COMPANY_CODE,ANY_VALUE(SOURCE_ENTITY),ANY_VALUE(SOURCE_ORG),PLANT_SK,ANY_VALUE(STD_PRICE_AMT_LCL),ANY_VALUE(PRICE_UNIT),ANY_VALUE(MOVING_PRICE_AMT_LCL),
  ANY_VALUE(COST_PER_UNIT_AMT_LCL),MATERIAL_NUM_SK,PROFIT_CENTER,STORAGE_LOCATION,VALUATION_CLASS,VALUATION_TYPE,SPL_STOCK_IND,LCL_CURR,
TO_CHAR(CURRENT_TIMESTAMP,'YYYY0MM') as FISCAL_PERIOD
 ,FISCAL_YEAR
,SUM(TOTAL_STOCK_QTY) as TOTAL_STOCK_QTY
,SUM(VALUATED_STOCK_QTY) as VALUATED_STOCK_QTY
,SUM(UNRESTRICTED_STOCK_QTY) as UNRESTRICTED_STOCK_QTY
,SUM(QI_STOCK_QTY) as QI_STOCK_QTY
,SUM(BLOCKED_STOCK_QTY) as BLOCKED_STOCK_QTY
,SUM(IN_TRANSIT_QTY) as IN_TRANSIT_QTY
,SUM(RET_STOCK_QTY) as RET_STOCK_QTY
,SUM(CUST_CONSIGNED_QTY) as CUST_CONSIGNED_QTY
,SUM(VENDOR_CONSIGNED_QTY) as VENDOR_CONSIGNED_QTY
,SUM(RESTRICTED_STOCK_QTY) as RESTRICTED_STOCK_QTY
,SUM(TOTAL_STOCK_AMT_LCL) as TOTAL_STOCK_AMT_LCL
,SUM(VALUATED_STOCK_AMT_LCL) as VALUATED_STOCK_AMT_LCL
,SUM(UNRESTRICTED_STOCK_AMT_LCL) as UNRESTRICTED_STOCK_AMT_LCL
,SUM(QI_STOCK_AMT_LCL) as QI_STOCK_AMT_LCL
,SUM(BLOCKED_STOCK_AMT_LCL) as BLOCKED_STOCK_AMT_LCL
,SUM(IN_TRANSIT_AMT_LCL) as IN_TRANSIT_AMT_LCL
,SUM(RET_STOCK_AMT_LCL) as RET_STOCK_AMT_LCL
,SUM(CUST_CONSIGNED_AMT_LCL) as CUST_CONSIGNED_AMT_LCL
,SUM(VENDOR_CONSIGNED_AMT_LCL) as VENDOR_CONSIGNED_AMT_LCL
,SUM(RESTRICTED_STOCK_AMT_LCL) AS RESTRICTED_STOCK_AMT_LCL
,CURRENT_TIMESTAMP::timestamp AS INV_LOAD_TS
FROM "TEST_DB"."ISC_TARGET"."FACT_INVENTORY" 
WHERE FISCAL_PERIOD <= (SELECT TO_CHAR(CURRENT_TIMESTAMP,'YYYY0MM') as FISCAL_PERIOD FROM "TEST_DB"."ISC_TARGET"."TIME_FISCAL_PERIOD" CLNDR WHERE CLNDR.FISCAL_PERIOD_END_STR = TO_CHAR(CURRENT_TIMESTAMP,'YYYYMMDD'))
GROUP BY SOURCE_SYSTEM,FISCAL_YEAR,COMPANY_CODE,PLANT_SK,PROFIT_CENTER,MATERIAL_NUM_SK,STORAGE_LOCATION,LCL_CURR,VALUATION_CLASS,VALUATION_TYPE,SPL_STOCK_IND);
"""
default_args = {
    'owner': 'ntt',
    'start_date': dt.datetime(year=2020, month=7, day=5),
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
    'depends_on_past': False,
    'email': ['sindhura.bhattam@nttdata.com'],
    'email_on_failure': True
}
with DAG(dag_id=DAG_NAME, default_args=default_args, catchup=False,
         schedule_interval=SCHEDULE_INTERVAL) as dag:
    build_tasks(source_container=SOURCE_CONTAINER,
                source_file_path=SOURCE_FILE_PATH,
                archive_container=ARCHIVE_CONTAINER,
                sql_tasks_info=sql_tasks_info,
                wasb_conn=WASB_CONNECTION_NAME,
                snowflake_conn=SNOWFLAKE_CONNECTION_NAME)
