import sys
import airflow
from airflow import DAG
import datetime as dt
from collections import OrderedDict
from airflow_template_helper.helper import build_tasks
from airflow_template_helper import constants

DAG_NAME = "corp_exchange_rate_dag"
SCHEDULE_INTERVAL = "@once"
# SCHEDULE_INTERVAL = "*/10 * * * *"
SNOWFLAKE_CONNECTION_NAME = "snowflake_conn"
WASB_CONNECTION_NAME = "wasb_conn"
SOURCE_CONTAINER = "snowflake-stage"
ARCHIVE_CONTAINER = "snowflake-stage-processed"
SOURCE_FILE_PATH = "citibike/"  # path from where the files are loaded into snowflake line2

SOURCE_TCURR = "SRC_SAPECC_BRP.TCURR"
SOURCE_TCURF = "SRC_SAPECC_BRP.TCURF"
STAGING_TABLE = "SRC_HANA_BHP.EX_RATE_TRANS"
TARGET_TABLE = "CORP_REF.EXCHANGE_RATE"



sql_tasks_info = OrderedDict()
sql_tasks_info["create_temp_table"] = """ 
CREATE OR REPLACE TRANSIENT TABLE {0} AS  (
 SELECT
      ROW_NUMBER() OVER (ORDER BY KURST,
      FCURR,
      TCURR,
      MANDT,                   
      GDATU desc) ROW_NUM,
      MANDT CLIENT,
      KURST RATE_TYPE,
      FCURR ,
      TCURR ,
      trunc(99999999 - GDATU) START_DATE,
      (case when UKURS <0 then (1/UKURS)*(-1)
      else UKURS end) EX_RATE
      FROM
      {1}
)
""".format(STAGING_TABLE,SOURCE_TCURR)

sql_tasks_info["merge_into_corp_master"] = """
MERGE INTO {1} AS T 
USING (
       SELECT 
        A.DATE START_DATE,
        NVL(to_number(to_char(DATEADD(day,-1,TO_DATE(TO_VARCHAR(B.DATE),'YYYYMMDD')),'yyyymmdd'),'99999999'),99991231) END_DATE,
        A.FCURR,
        A.TCURR,
        A.RATE_TYPE,
        ROUND((A.EX_RATE * F.TFACT)/F.FFACT,5) EX_RATE,
        F.FFACT,
        F.TFACT,
        CURRENT_TIMESTAMP::TIMESTAMP LOAD_TS,
        CURRENT_TIMESTAMP::TIMESTAMP UPDATE_TS
        FROM 
        {0} A LEFT JOIN {0} B ON ((A.ROWNUM = (B.ROWNUM-1))
          and (A.FCURR = B.FCURR) 
          AND (A.TCURR = B.TCURR) 
          AND (A.RATE_TYPE = B.RATE_TYPE))
        LEFT JOIN {3} F ON (A.CLIENT = F.MANDT AND A.TCURR = F.TCURR AND A.FCURR = F.FCURR AND A.RATE_TYPE = F.KURST)
        --where A.FCURR = 'INR' AND A.RATE_TYPE = 'A'
        ORDER BY 
        A.FCURR,
        A.TCURR,
        A.DATE,
        A.RATE_TYPE 
       ) S ON ((T.START_DATE = S.START_DATE)  AND (T.SOURCE_CURRENCY = S.FCURR) AND (T.TARGET_CURRENCY = S.TCURR) AND (T.EXCHANGE_RATE_TYPE = S.RATE_TYPE))
 WHEN MATCHED and ((T.END_DATE != S.END_DATE) OR T.EXCHANGE_RATE != S.EX_RATE) THEN DELETE
 WHEN MATCHED and ((T.END_DATE != S.END_DATE) OR T.EXCHANGE_RATE != S.EX_RATE) THEN UPDATE SET 
                                                                                T.END_DATE = S.END_DATE,T.EXCHANGE_RATE = S.EX_RATE, T.ER_LOAD_TS = S.LOAD_TS, T.ER_UPDATE_TS = S.UPDATE_TS
 WHEN NOT MATCHED THEN INSERT (START_DATE,END_DATE,SOURCE_CURRENCY,TARGET_CURRENCY,EXCHANGE_RATE_TYPE,EXCHANGE_RATE,ER_LOAD_TS,ER_UPDATE_TS) 
                       VALUES (S.START_DATE,S.END_DATE,S.FCURR,S.TCURR,S.RATE_TYPE,S.EX_RATE,S.LOAD_TS,S.UPDATE_TS)
""".format(STAGING_TABLE,TARGET_TABLE,SOURCE_TCURR,SOURCE_TCURF)


default_args = {
    'owner': 'honeywelledwcorp',
    'start_date': dt.datetime(year=2020, month=4, day=28),
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
    'depends_on_past': False,
    'email': ['manoj.jayamangala@honeywell.com'],
    'email_on_failure': True
    }

with DAG(dag_id=DAG_NAME, default_args=default_args, catchup=False,
         schedule_interval=SCHEDULE_INTERVAL) as dag:
    build_tasks(source_container=SOURCE_CONTAINER,
                source_file_path=SOURCE_FILE_PATH,
                archive_container=ARCHIVE_CONTAINER,
                sql_tasks_info=sql_tasks_info,
                wasb_conn=WASB_CONNECTION_NAME,
                snowflake_conn=SNOWFLAKE_CONNECTION_NAME)
