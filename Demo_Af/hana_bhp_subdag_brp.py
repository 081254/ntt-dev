from airflow.models import DAG
import datetime as dt
from collections import OrderedDict
from airflow_template_helper.helper import build_tasks
from airflow_template_helper import constants
from airflow_template_helper.constants import SNOWFLAKE_CONNECTION_NAME, WASB_CONNECTION_NAME, SOURCE_CONTAINER, \
    ARCHIVE_CONTAINER, STAGE_NAME

SUBDAG_NAME = "hana_bhp_subdag_brp"
SOURCE_FILE_PATH = ""
SOURCE_BASE_PATH = ""
ARCHIVE_BASE_PATH = ""

sql_tasks_info = OrderedDict()
sql_tasks_info["Merge_data_Trns_to_Stg"] = """
MERGE INTO HON_ENT_SAP_SOURCE_DB.PUBLIC.QV_INV_DETAIL_INFA_Stg AS Stg 
USING HON_ENT_SAP_SOURCE_DB.SAP.QV_INV_DETAIL_INFA_TRNS AS Trns
 on Trns.SOURCE_SYSTEM = Stg.SOURCE_SYSTEM and Trns.MATNR = Stg.MATNR and Trns.MANDT = Stg.MANDT and Trns.FISCPER = Stg.FISCPER and Trns.FISCAL_YEAR_WEEK_STR = Stg.FISCAL_YEAR_WEEK_STR
   and Trns.BUKRS = Stg.BUKRS and Trns.WERKS = Stg.WERKS and Trns.PRCTR = Stg.PRCTR and ifnull(Trns.SOBKZ,'') = ifnull(Stg.SOBKZ,'')
   and ifnull(Trns.BWTAR,'') = ifnull(Stg.BWTAR,'') and ifnull(Trns.WAERS,'') = ifnull(Stg.WAERS,'')
 WHEN MATCHED AND
     (
       Stg.MD5 <> Trns.MD5
     )
     THEN UPDATE SET
        Stg.CM_UNREST_STOCK_QTY = Trns.CM_UNREST_STOCK_QTY,
        Stg.CM_QI_STOCK_QTY = Trns.CM_QI_STOCK_QTY,
        Stg.CM_BLK_STK_QTY = Trns.CM_BLK_STK_QTY,
        Stg.CM_CUST_CONSG_TOT_QTY = Trns.CM_CUST_CONSG_TOT_QTY,
        Stg.CM_VEND_CONSG_TOT_QTY = Trns.CM_VEND_CONSG_TOT_QTY,
        Stg.CM_SIT_STOCK_QTY = Trns.CM_SIT_STOCK_QTY,
        Stg.CM_UNREST_STOCK_VALUE = Trns.CM_UNREST_STOCK_VALUE,
        Stg.CM_QI_STOCK_VALUE = Trns.CM_QI_STOCK_VALUE,
        Stg.CM_BLK_STK_VALUE = Trns.CM_BLK_STK_VALUE,
        Stg.CM_CUST_CONSG_TOT_VALUE = Trns.CM_CUST_CONSG_TOT_VALUE,
        Stg.CM_VEND_CONSG_TOT_VALUE = Trns.CM_VEND_CONSG_TOT_VALUE,
        Stg.CM_SIT_STOCK_VALUE = Trns.CM_SIT_STOCK_VALUE,
        Stg.SNAPSHOT_DTH = Trns.SNAPSHOT_DTH,
        Stg.MD5 = Trns.MD5,
        Stg.UPDATE_PROCESS_TS = current_timestamp::timestamp
 WHEN NOT MATCHED
     THEN INSERT (
        SOURCE_SYSTEM
        ,MANDT
        ,FISCPER
        ,FISCAL_YEAR_WEEK_STR
        ,BUKRS
        ,WERKS
        ,PRCTR
        ,MATNR
        ,SOBKZ
        ,BWTAR
        ,WAERS
        ,CM_UNREST_STOCK_QTY
        ,CM_QI_STOCK_QTY
        ,CM_BLK_STK_QTY
        ,CM_CUST_CONSG_TOT_QTY
        ,CM_VEND_CONSG_TOT_QTY
        ,CM_SIT_STOCK_QTY
        ,CM_UNREST_STOCK_VALUE
        ,CM_QI_STOCK_VALUE
        ,CM_BLK_STK_VALUE
        ,CM_CUST_CONSG_TOT_VALUE
        ,CM_VEND_CONSG_TOT_VALUE
        ,CM_SIT_STOCK_VALUE
        ,SNAPSHOT_DTH
        ,MD5
        ,CREATE_PROCESS_TS
       )
      values(
        Trns.SOURCE_SYSTEM
        ,Trns.MANDT
        ,Trns.FISCPER
        ,Trns.FISCAL_YEAR_WEEK_STR
        ,Trns.BUKRS
        ,Trns.WERKS
        ,Trns.PRCTR
        ,Trns.MATNR
        ,ifnull(Trns.SOBKZ,'')
        ,ifnull(Trns.BWTAR,'')
        ,Trns.WAERS
        ,Trns.CM_UNREST_STOCK_QTY
        ,Trns.CM_QI_STOCK_QTY
        ,Trns.CM_BLK_STK_QTY
        ,Trns.CM_CUST_CONSG_TOT_QTY
        ,Trns.CM_VEND_CONSG_TOT_QTY
        ,Trns.CM_SIT_STOCK_QTY
        ,Trns.CM_UNREST_STOCK_VALUE
        ,Trns.CM_QI_STOCK_VALUE
        ,Trns.CM_BLK_STK_VALUE
        ,Trns.CM_CUST_CONSG_TOT_VALUE
        ,Trns.CM_VEND_CONSG_TOT_VALUE
        ,Trns.CM_SIT_STOCK_VALUE
        ,Trns.SNAPSHOT_DTH
        ,Trns.MD5
        ,current_timestamp::TIMESTAMP
);
"""
sql_tasks_info["Merge_data_Stg_to_Tgt"] = """
MERGE INTO HON_ENT_SAP_SOURCE_DB.PUBLIC.QV_INV_DETAIL_INFA_TGT AS Tgt
USING HON_ENT_SAP_SOURCE_DB.PUBLIC.QV_INV_DETAIL_INFA_STG AS Stg
on Stg.SOURCE_SYSTEM = Tgt.SOURCE_SYSTEM and Stg.MATNR = Tgt.MATNR and Stg.MANDT = Tgt.MANDT and Stg.FISCPER = Tgt.FISCPER and Stg.FISCAL_YEAR_WEEK_STR = Tgt.FISCAL_YEAR_WEEK_STR
and Stg.BUKRS = Tgt.BUKRS and Stg.WERKS = Tgt.WERKS and Stg.PRCTR = Tgt.PRCTR and ifnull(Stg.SOBKZ,'') = ifnull(Tgt.SOBKZ,'')
   and ifnull(Stg.BWTAR,'') = ifnull(Tgt.BWTAR,'') and ifnull(Stg.WAERS,'') = ifnull(Tgt.WAERS,'')
 WHEN MATCHED AND
     (
       Tgt.MD5 <> Stg.MD5
      )
     THEN UPDATE SET
        Tgt.CM_UNREST_STOCK_QTY = Stg.CM_UNREST_STOCK_QTY,
        Tgt.CM_QI_STOCK_QTY = Stg.CM_QI_STOCK_QTY,
        Tgt.CM_BLK_STK_QTY = Stg.CM_BLK_STK_QTY,
        Tgt.CM_CUST_CONSG_TOT_QTY = Stg.CM_CUST_CONSG_TOT_QTY,
        Tgt.CM_VEND_CONSG_TOT_QTY = Stg.CM_VEND_CONSG_TOT_QTY,
        Tgt.CM_SIT_STOCK_QTY = Stg.CM_SIT_STOCK_QTY,
        Tgt.CM_UNREST_STOCK_VALUE = Stg.CM_UNREST_STOCK_VALUE,
        Tgt.CM_QI_STOCK_VALUE = Stg.CM_QI_STOCK_VALUE,
        Tgt.CM_BLK_STK_VALUE = Stg.CM_BLK_STK_VALUE,
        Tgt.CM_CUST_CONSG_TOT_VALUE = Stg.CM_CUST_CONSG_TOT_VALUE,
        Tgt.CM_VEND_CONSG_TOT_VALUE = Stg.CM_VEND_CONSG_TOT_VALUE,
        Tgt.CM_SIT_STOCK_VALUE = Stg.CM_SIT_STOCK_VALUE,
        Tgt.SNAPSHOT_DTH = Stg.SNAPSHOT_DTH,
        Tgt.MD5 = Stg.MD5,
        Tgt.UPDATE_PROCESS_TS = current_timestamp::timestamp
 WHEN NOT MATCHED
     THEN INSERT (
        SOURCE_SYSTEM
        ,MANDT
        ,FISCPER
        ,FISCAL_YEAR_WEEK_STR
        ,BUKRS
        ,WERKS
        ,PRCTR
        ,MATNR
        ,SOBKZ
        ,BWTAR
        ,WAERS
        ,CM_UNREST_STOCK_QTY
        ,CM_QI_STOCK_QTY
        ,CM_BLK_STK_QTY
        ,CM_CUST_CONSG_TOT_QTY
        ,CM_VEND_CONSG_TOT_QTY
        ,CM_SIT_STOCK_QTY
        ,CM_UNREST_STOCK_VALUE
        ,CM_QI_STOCK_VALUE
        ,CM_BLK_STK_VALUE
        ,CM_CUST_CONSG_TOT_VALUE
        ,CM_VEND_CONSG_TOT_VALUE
        ,CM_SIT_STOCK_VALUE
        ,SNAPSHOT_DTH
        ,MD5
        ,CREATE_PROCESS_TS
       )
      values(
        Stg.SOURCE_SYSTEM
        ,Stg.MANDT
        ,Stg.FISCPER
        ,Stg.FISCAL_YEAR_WEEK_STR
        ,Stg.BUKRS
        ,Stg.WERKS
        ,Stg.PRCTR
        ,Stg.MATNR
        ,ifnull(Stg.SOBKZ,'')
        ,ifnull(Stg.BWTAR,'')
        ,Stg.WAERS
        ,Stg.CM_UNREST_STOCK_QTY
        ,Stg.CM_QI_STOCK_QTY
        ,Stg.CM_BLK_STK_QTY
        ,Stg.CM_CUST_CONSG_TOT_QTY
        ,Stg.CM_VEND_CONSG_TOT_QTY
        ,Stg.CM_SIT_STOCK_QTY
        ,Stg.CM_UNREST_STOCK_VALUE
        ,Stg.CM_QI_STOCK_VALUE
        ,Stg.CM_BLK_STK_VALUE
        ,Stg.CM_CUST_CONSG_TOT_VALUE
        ,Stg.CM_VEND_CONSG_TOT_VALUE
        ,Stg.CM_SIT_STOCK_VALUE
        ,Stg.SNAPSHOT_DTH
        ,Stg.MD5
        ,current_timestamp::TIMESTAMP
);
"""

# Using
def build_subdag(master_dag, default_args, schedule_interval):
    dag_id = "{0}.{1}".format(master_dag, SUBDAG_NAME)
    with DAG(dag_id=dag_id,
             default_args=default_args,
             catchup=False,
             schedule_interval=schedule_interval) as dag:
        build_tasks(source_container=SOURCE_CONTAINER,
                    archive_container=ARCHIVE_CONTAINER,
                    source_file_path=SOURCE_FILE_PATH,
                    source_base_path=SOURCE_BASE_PATH,
                    archive_base_path=ARCHIVE_BASE_PATH,
                    sql_tasks_info=sql_tasks_info,
                    wasb_conn=WASB_CONNECTION_NAME,
                    snowflake_conn=SNOWFLAKE_CONNECTION_NAME,
                    include_blob_tasks=False)
        return dag
