
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.utils.decorators import apply_defaults
from airflow.exceptions import AirflowException
from airflow.hooks.http_hook import HttpHook
from airflow.models import Connection
from airflow.hooks.base_hook import BaseHook

"""
Extend Simple Http Operator with a callable function to formulate data. This data function will
be able to access the context to retrieve data such as task instance. This allow us to write cleaner 
code to handle HTTP response rather than writing one long template line to formulate the json data.
"""
class ExtendedHttpOperator(SimpleHttpOperator):
  

  def execute(self, context):
    self.context = context          
    http = HttpHook(self.method, http_conn_id=self.http_conn_id)

    #data_result = self.execute_callable(context)
    
    self.log.info("Calling HTTP method- ExtendedHttpOperator")
    #self.log.info("Post Data: {}".format(data_result))
    response = http.run(self.endpoint,
                        self.data,
                        self.headers,
                        self.extra_options)
    print("Inside ExtendedHttpOperator")
    if self.response_check:
        if not self.response_check(response):
            raise AirflowException("Response check returned False.")
    if self.xcom_push_flag:
        return response

  def execute_callable(self, context):
    return self.data(**context)