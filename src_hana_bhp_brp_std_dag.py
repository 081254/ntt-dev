from airflow.models import DAG
import datetime as dt
from airflow_template_helper import master_dag_helper
from airflow_template_helper import constants

# include corp in the mater dag, maintain the DAG_NAME convention, example below
DAG_NAME = "src_hana_bhp_brp_std_dag"
SCHEDULE_INTERVAL = "@once"
SUBDAGS_FOLDER = "hana"

default_args = {
    'owner': 'ntt',
    'start_date': dt.datetime(year=2020, month=6, day=16),
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
    'depends_on_past': False,
    'email': ['sindhura.bhattam@nttdata.com'],
    'email_on_failure': True
}
with DAG(dag_id=DAG_NAME,
         default_args=default_args,
         catchup=False,
         schedule_interval=SCHEDULE_INTERVAL,
         max_active_runs=1) as dag:
    master_dag_helper.build_master_subdags(master_dag_name=DAG_NAME,
                                           subdags_folder=SUBDAGS_FOLDER,
                                           default_args=default_args,
                                           schedule_interval=SCHEDULE_INTERVAL)