import airflow
from airflow.models import DAG
from airflow_template_helper.task_helper import build_sql_tasks_info
from airflow_template_helper.helper import build_tasks
from airflow_template_helper import constants
from airflow_template_helper.constants import SNOWFLAKE_CONNECTION_NAME, TASK_CONFIG_TABLE, SNOWFLAKE_CONNECTION_NAME, WASB_CONNECTION_NAME, SOURCE_CONTAINER, \
    ARCHIVE_CONTAINER, STAGE_NAME

DAG_NAME = "TASK_CONFIG_DEMO"
TASK_CONFIG_TABLE = "DEV.CORP_CONFIG.AIRFLOW_TASK_CONFIG"
SNOWFLAKE_CONNECTION_NAME = "snowflake_conn" 
SCHEDULE_INTERVAL = "@once"
SOURCE_FILE_PATH = "doesnt matter"

args = {
    'owner': 'Marlon',
    'start_date': airflow.utils.dates.days_ago(2),
}

with DAG(
        dag_id=DAG_NAME, 
        default_args=args, 
        catchup=False,
        schedule_interval=SCHEDULE_INTERVAL) as dag:
    build_tasks(source_container=SOURCE_CONTAINER,
                source_file_path=SOURCE_FILE_PATH,
                archive_container=ARCHIVE_CONTAINER,
                sql_tasks_info=build_sql_tasks_info(DAG_NAME,TASK_CONFIG_TABLE,SNOWFLAKE_CONNECTION_NAME,prod_move=False),
                wasb_conn=WASB_CONNECTION_NAME,
                include_blob_tasks=False,
                snowflake_conn=SNOWFLAKE_CONNECTION_NAME)

