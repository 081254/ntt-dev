from airflow.models import DAG
import datetime as dt
from collections import OrderedDict
from airflow_template_helper.helper import build_tasks


DAG_NAME = "new_dag_test"
# SCHEDULE_INTERVAL = "@once"
SNOWFLAKE_CONNECTION_NAME = "snowflake_conn"
WASB_CONNECTION_NAME = "wasb_conn"
SOURCE_CONTAINER = "airflow-test"
ARCHIVE_CONTAINER = "airflow-test"
SCHEDULE_INTERVAL = "@once"
SOURCE_FILE_PATH = "src_hana_bhp/inventory/QV_INV_DETAIL_ADLSGEN2"  # path from where the files are loaded into snowflake, data file is <<view_name>>_timestamp.csv (change to pipe delimited)

sql_tasks_info = OrderedDict()
sql_tasks_info["create_transient_table"] ="""
CREATE OR REPLACE TABLE "HON_ENT_SAP_SOURCE_DB"."PUBLIC"."QV_INV_DETAIL_INFA_TRNS_test" ( 
SOURCE_SYSTEM NVARCHAR(30) ,
MANDT NVARCHAR(3) ,
FISCPER NVARCHAR(5000) ,
FISCAL_YEAR_WEEK_STR NVARCHAR(6) ,
BUKRS NVARCHAR(15) ,
WERKS NVARCHAR(12) ,
PRCTR VARCHAR(10) ,
MATNR NVARCHAR(90) ,
SOBKZ NVARCHAR ,
BWTAR NVARCHAR(30) ,
WAERS NVARCHAR(5) ,
CM_UNREST_STOCK_QTY DECIMAL(13,3) ,
CM_QI_STOCK_QTY DECIMAL(13,3) ,
CM_BLK_STK_QTY DECIMAL(13,3) ,
CM_CUST_CONSG_TOT_QTY DECIMAL(20,3) ,
CM_VEND_CONSG_TOT_QTY DECIMAL(13,3) ,
CM_SIT_STOCK_QTY DECIMAL(20,3) ,
CM_UNREST_STOCK_VALUE DECIMAL(34,7) ,
CM_QI_STOCK_VALUE DECIMAL(34,7) ,
CM_BLK_STK_VALUE DECIMAL(34,7) ,
CM_CUST_CONSG_TOT_VALUE DECIMAL(34,7) ,
CM_VEND_CONSG_TOT_VALUE DECIMAL(34,7) ,
CM_SIT_STOCK_VALUE DECIMAL(34,7) ,
SNAPSHOT_DTH NVARCHAR(30),
MD5 NVARCHAR(5000),
CREATE_PROCESS_TS TIMESTAMP_NTZ(9),
CONSTRAINT PK_QV_INV_DETAIL_INFA_TRNS primary key (SOURCE_SYSTEM,MANDT,FISCPER,FISCAL_YEAR_WEEK_STR,BUKRS,WERKS,PRCTR,MATNR,SOBKZ,BWTAR,WAERS)
);
"""

sql_tasks_info["Load_data_ADLS_to_Trns"] = """
COPY INTO "HON_ENT_SAP_SOURCE_DB"."PUBLIC"."QV_INV_DETAIL_INFA_TRNS_test"
    FROM (
    SELECT $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,
    current_timestamp:: timestamp
    FROM
    @PUBLIC.CORP_ADLS_SI_STAGE_LANDING/src_hana_bhp/inventory/QV_INV_DETAIL_ADLSGEN2
    )
FILE_FORMAT = (FORMAT_NAME = PUBLIC.CSV_SEMI_COLON)
"""
default_args = {
    'owner': 'ntt',
    'start_date': dt.datetime(year=2020, month=6, day=12),
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
    'depends_on_past': False,
    'email': ['sindhura.bhattam@nttdata.com'],
    'email_on_failure': True
}
with DAG(dag_id=DAG_NAME, default_args=default_args, catchup=False,
         schedule_interval=SCHEDULE_INTERVAL) as dag:
    build_tasks(source_container=SOURCE_CONTAINER,
                source_file_path=SOURCE_FILE_PATH,
                archive_container=ARCHIVE_CONTAINER,
                sql_tasks_info=sql_tasks_info,
                wasb_conn=WASB_CONNECTION_NAME,
                snowflake_conn=SNOWFLAKE_CONNECTION_NAME)
