from airflow.models import DAG
import datetime as dt
from collections import OrderedDict
from airflow_template_helper.helper import build_tasks
from airflow_template_helper.constants import SNOWFLAKE_CONNECTION_NAME, WASB_CONNECTION_NAME,SOURCE_CONTAINER,ARCHIVE_CONTAINER,STAGE_NAME

DAG_NAME = "Snowflake_Test_NTT104205"
SCHEDULE_INTERVAL = "@once"
#SCHEDULE_INTERVAL = "*/10 * * * *"

SOURCE_FILE_PATH = "src_hana_bhp/inventory/"  # path from where the files are loaded into snowflake
STAGING_TABLE = "HON_ENT_SAP_SOURCE_DB.QV_INV_DETAIL_INFA_TRNS"

sql_tasks_info = OrderedDict()
sql_tasks_info["load_QV_INV_DETAIL_INFA_TRNS"] = """copy into {0}
FROM
(select $1, $2, $3 from @DEV.corp_stgSELECT $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,
    current_timestamp:: timestamp
    FROM
    @PUBLIC.{2}/{1}) 
file_format = (format_name='DEV.corp_stg.FF_CSV_PIPE')""".format(STAGING_TABLE, SOURCE_FILE_PATH, STAGE_NAME)

default_args = {
    'owner': 'NTTT',
    'start_date': dt.datetime(year=2020, month=6, day=1, hour=5),
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
    'depends_on_past': False,
     'email': ['abc@honeywell.com'],
    'email_on_failure': True
}


with DAG(dag_id=DAG_NAME, default_args=default_args, catchup=False,
         schedule_interval=SCHEDULE_INTERVAL) as dag:
    build_tasks(source_container=SOURCE_CONTAINER,
                source_file_path=SOURCE_FILE_PATH,
                archive_container=ARCHIVE_CONTAINER,
                sql_tasks_info=sql_tasks_info,
                wasb_conn=WASB_CONNECTION_NAME,
                snowflake_conn=SNOWFLAKE_CONNECTION_NAME)
