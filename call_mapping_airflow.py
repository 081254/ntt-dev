import airflow
from airflow import DAG
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.sensors import HttpSensor
from datetime import datetime, timedelta
from custom_plugin.operators.ext_http_operator import ExtendedHttpOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator, DagRunOrder
import json



default_args = {
    'owner': 'Airflow',
    'depends_on_past': False,
    'start_date': datetime(2020, 5, 5),
    'email': 'informer@honeywell.com',
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=3),
}

dag = DAG('call-mapping',
    catchup=False,
    schedule_interval=None,
	start_date=datetime(2020, 5, 5),
    default_args=default_args)
	
	# Returns the payload that need to be passed to API
def get_payload(**context):
    #dag_run_conf = json.loads(context['dag_run'].conf)
    data=json.dumps({"action": "RunDeployedMapping", "customParameters": [ { "name": "DOMAIN_NAME", "value": "DOM_DEI_HONEYWELL_NONPROD" }, { "name": "DIS_NAME", "value": "DEI_DIS_NONPROD" }, { "name": "USERNAME", "value": "H362657" }, { "name": "PASSWORD", "value": "Ramesh@1981" }, { "name": "APPLICATION_NAME", "value": "Application_wf_ISC_INV_HANA_ST_INV_DETAIL_ORACLE_IGS_FULL_LOAD" }, { "name": "WORKFLOW_NAME", "value": "wf_ISC_INV_HANA_ST_INV_DETAIL_ORACLE_IGS_FULL_LOAD" } ]})
    return data

	
	#call ExtendedHttpOperator as SimpleHTTPOperator dont allow you to handle API response
	#pass method, endpoint, headers and data as per your HTTP request 
call_api = ExtendedHttpOperator(
    task_id='call_api',
    method='POST',
    http_conn_id='informatica_api',
	
    endpoint='/DataIntegrationService/modules/ms/v1/applications/App_m_trigger_workflow6/mappings/m_trigger_workflow',
    headers={'accept': 'application/json', 'Content-Type': 'application/json', 'securitydomain': 'AADDC', 'username': 'H362657', 'encryptedpassword': 'nWHObONzsWFBbqLzJSGiVA==', 'servicename': 'DEI_DIS_NONPROD'},
    data=json.dumps({"action": "RunDeployedMapping", "customParameters": [ { "name": "DOMAIN_NAME", "value": "DOM_DEI_HONEYWELL_NONPROD" }, { "name": "DIS_NAME", "value": "DEI_DIS_NONPROD" }, { "name": "USERNAME", "value": "H362657" }, { "name": "DOMAIN", "value": "AADDC" }, { "name": "PASSWORD", "value": "Ramesh@1981" }, { "name": "APPLICATION_NAME", "value": "Application_wf_ISC_INV_HANA_ST_INV_DETAIL_ORACLE_IGS_FULL_LOAD" }, { "name": "WORKFLOW_NAME", "value": "wf_ISC_INV_HANA_ST_INV_DETAIL_ORACLE_IGS_FULL_LOAD" } ]}),
	
    xcom_push=True,
    dag=dag)

	#callable implementation to read API response and decide whether its a success or error
def check_api_response_callable(ti, **context):
    response = ti.xcom_pull(task_ids='call_api')
    print("here it is")
    returncode = response.status_code
    response_type = 'Success' if returncode == 200 else 'Error'
    #response_type = json.loads(response).get("status")
    print(returncode)
    print(response_type)
    return {
        'Success': 'success',
        'Error': 'handle_api_error'
    }.get(response_type, '')

	# BranchPythonOperator to implement success or failure
check_api_response = BranchPythonOperator(
    task_id='check_api_response',
    python_callable=check_api_response_callable,
    provide_context=True,
    dag=dag
  )
  
# dummy operator for success implementation.
# you can put your custom code as per your success action.
success = DummyOperator(task_id='success')

# Handle error in a callable.
# It defines what needs to be done in case of API failure
def handle_api_error_callable(context, dro):

    task_instance = context['task_instance']
    response = task_instance.xcom_pull(task_ids='call_api')

    time_now = datetime.now()  # Use Current Time as you want to keep run_id unique
    dro.run_id = 'COLLECTION_{0}_Handle_Error'.format(time_now.strftime('%y%m%d%H%M%S'))
    print("Some error returned by API")
    print(response.text)

    return dro

# This operator defines the behaviour of API call failure handling
# This calls whole flow again, you can choose another operator if you dont want to call whole flow again. 
handle_api_error = TriggerDagRunOperator(
    task_id='handle_api_error',
    trigger_dag_id='call-mapping',
    python_callable=handle_api_error_callable
)

call_api >> check_api_response >> [handle_api_error, success]