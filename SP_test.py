from airflow.models import DAG
import datetime as dt
from collections import OrderedDict
from airflow_template_helper.helper import build_tasks
from airflow_template_helper import constants
from airflow_template_helper.constants import SNOWFLAKE_CONNECTION_NAME, WASB_CONNECTION_NAME, SOURCE_CONTAINER, \
    ARCHIVE_CONTAINER, STAGE_NAME

DAG_NAME = "SP_test"
# SCHEDULE_INTERVAL = "@once"
SCHEDULE_INTERVAL = "@once"
SOURCE_FILE_PATH=""

sql_tasks_info = OrderedDict()
sql_tasks_info["create_SP_ADHOC_MONTH_SNAPSHOT"] ="""
CREATE OR REPLACE PROCEDURE "PUBLIC"."SP_ADHOC_MONTH_SNAPSHOT"()
RETURNS FLOAT
LANGUAGE JAVASCRIPT
EXECUTE AS OWNER
AS '  
    var i =1;
    var my_sql_command = "select PERIOD from TEST.FISCAL_MONTH_END where flag =''Y''";
    var statement1 = snowflake.createStatement( {sqlText: my_sql_command} );
    var result_set1 = statement1.execute(1);
    // Loop through the PERIODS, processing one PERIOD at a time... 
    while (result_set1.next())  {
       var column1 = result_set1.getColumnValue(1);
	     snowflake.execute( { sqlText:`DELETE FROM TEST.FACT_TEST_RPT WHERE FISCAL_PERIOD = ${column1}`
       } );
         snowflake.execute( { sqlText:`insert into TEST.FACT_TEST_RPT (MATERIAL,PLANT,STORAGE_LOACATION,QTY,Fiscal_Period) 
		 select   MATERIAL,PLANT,STORAGE_LOACATION,SUM(QTY) as QTY,${column1} as fiscal_period from  TEST.FACT_TEST  where fiscal_period <= ${column1} 
		 group by MATERIAL,PLANT,STORAGE_LOACATION`
			   } );
	   i=i+1;
       }
  return 0.0; // Replace with something more useful.
  ';
"""
sql_tasks_info["call_SP_ADHOC_MONTH_SNAPSHOT"] ="""
call PUBLIC.SP_ADHOC_MONTH_SNAPSHOT();
"""
default_args = {
    'owner': 'ntt',
    'start_date': dt.datetime(year=2020, month=7, day=5),
    'retries': 1,
    'retry_delay': dt.timedelta(minutes=5),
    'depends_on_past': False,
    'email': ['sindhura.bhattam@nttdata.com'],
    'email_on_failure': True
}
with DAG(dag_id=DAG_NAME, default_args=default_args, catchup=False,
         schedule_interval=SCHEDULE_INTERVAL) as dag:
    build_tasks(source_container=SOURCE_CONTAINER,
                source_file_path=SOURCE_FILE_PATH,
                archive_container=ARCHIVE_CONTAINER,
                sql_tasks_info=sql_tasks_info,
                wasb_conn=WASB_CONNECTION_NAME,
                snowflake_conn=SNOWFLAKE_CONNECTION_NAME)
