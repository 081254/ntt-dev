import logging
import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.snowflake_hook import SnowflakeHook

#Defining our default arguments
args = {
    'owner': 'Airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['marlonholland7@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    #'retries': 1,
    #'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
    # 'wait_for_downstream': False,
    # 'dag': dag,
    # 'sla': timedelta(hours=2),
    # 'execution_timeout': timedelta(seconds=300),
    # 'on_failure_callback': some_function,
    # 'on_success_callback': some_other_function,
    # 'on_retry_callback': another_function,
    # 'sla_miss_callback': yet_another_function,
}

#Creating our dag object
dag = DAG(
    dag_id="azure_to_snflk_demo", 
    default_args=args, 
    schedule_interval=None
)

#Example use of a Snowflake hook to grab data from Snowflake.
snowflake_hook = SnowflakeHook(snowflake_conn_id="snflk_conn_ntt")

def execute_sql(**context):
	conn = snowflake_hook.get_conn()
	cur = conn.cursor()
	try:
		# Drop the table if already exists in the database.
		cur.execute("DROP TABLE IF EXISTS HON_ENT_SAP_SOURCE_DB.STAGING.ERP_CUST_SOLD_TO_PRE_STAGE")
		# Create the table definition for pre-stage
		cur.execute("CREATE OR REPLACE TABLE HON_ENT_SAP_SOURCE_DB.STAGING.ERP_CUST_SOLD_TO_PRE_STAGE (COMP_CD VARCHAR(255), SALE_ORG_CD VARCHAR(255), SOLD_TO_CUST_NUM VARCHAR(255), CUST_NM VARCHAR(255), SALE_CHNL_NM VARCHAR(255), INVC_ISO_CURNCY_CD VARCHAR(255), CUST_VAT_NUM VARCHAR(255), POS_PLATFM_VALUE VARCHAR(255), PSS_CHNL_VALUE VARCHAR(255), PPP_CLS_NM VARCHAR(255), SALE_OFFC_NM VARCHAR(255), SALE_GRP_SIP_AREA_NM VARCHAR(255), LCL_HEAD_QTR_NM VARCHAR(255), REGN_HEAD_QTR_NM VARCHAR(255), GLOBAL_HEAD_QTR_NM VARCHAR(255), SOLD_TO_ADDR_DESC VARCHAR(255), SOLD_TO_ZIP_CD VARCHAR(255), SOLD_TO_CITY_NM VARCHAR(255), SOLD_TO_CNTRY_CD VARCHAR(255), SALE_DSTR_NM VARCHAR(255));")
		# Copy the data from Amazon S3 bucket to Snowflake database Pre-stage table
		print("Table Creation completed successfully")
	except Exception as e:
		raise e

	finally:
		cur.close()

		
#Using a with statement to execute cleanup code
with dag:

    #Using a snowflake operator to execute a select command on our raw data in Azure
    execute_sql = PythonOperator(
        task_id = "execute_sql",
        python_callable = execute_sql
    )	
    
execute_sql
